<?php
/*include_once 'debug.php';*/

// connect à la db
$host = '127.0.0.1';
$db   = 'kobe';
$user = 'root';
$pass = ''; // ne pas faire ça dans la vrai vie !

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

session_start();

function createburger($id, $imgURL, $descri, $prix){
    global $pdo;
    $req = $pdo->prepare('insert into burger (id, imgURL, descri, prix) values (?, ?, ?, ?);');
    $req->execute([$id, $imgURL, $descri, $prix]);
}

function readAllburger(){
    global $pdo;
    $req = $pdo->query("select * from burger;");
    return $req->fetchAll();
}

function readburger($id){
    global $pdo;
    $req = $pdo->prepare("select * from burger where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}

function deleteburger($id){
    global $pdo;
    $req = $pdo->prepare("delete from burger where id=?;");
    $req->execute([$id]);
}

function updateburger($id, $imgURL, $titre, $descri, $prix){
    global $pdo;
    $req = $pdo->prepare("update burger set imgURL=?, titre=?, descri=? , prix=? where id=?;");
    $req->execute([$imgURL, $titre, $descri, $prix, $id]);
}

function createresa($id, $resdate, $mail, $nombre){
    global $pdo;
    $req = $pdo->prepare('insert into reserv (id, resdate, mail, nombre) values (?, ?, ?, ?);');
    $req->execute([$id, $resdate, $mail, $nombre]);
}

function readAllresa(){
    global $pdo;
    $req = $pdo->query("select * from reserv;");
    return $req->fetchAll();
}

function readresa($id){
    global $pdo;
    $req = $pdo->prepare("select * from reserv where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
}

function deleteresar($id){
    global $pdo;
    $req = $pdo->prepare("delete from reserv where id=?;");
    $req->execute([$id]);
}

function updateresa($id, $resdate, $mail, $nombre){
    global $pdo;
    $req = $pdo->prepare("update reserv set resdate=?, mail=? , nombre=? where id=?;");
    $req->execute([$resdate, $mail, $nombre, $id]);
}
   
$horaire = [
    "Lundi" => [
        "09h a 14h",
        '18H à 22h',
    ],
    "Mardi" => [
        "09h a 14h",
        '18H à 22h',
    ],
     "Mercredi" => [
        "09h a 14h",
        '18H à 22h',
     ],
      "Jeudi" => [
        "09h a 14h",
        '18H à 22h',
      ],
      "Vendredi" => [
        "09h a 14h",
        '18H à 22h',
      ],
      "Samedi" => [
        "09h a 14h",
        '18H à 22h',
      ],
      "Dimanche" => [
        "09h a 14h",
        '18H à 22h',
      ],
    ];
    $jour = [
        'Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'
    ];
function horaire($horaire, $jour){
    $i = 0;
    foreach ($horaire as list($matin, $soir)){
        $lej = $jour[$i];
        $i++;
        echo "  <div class='br'>
                <div class='cb'>$lej</div>
                <div class='hor'>
                <span class='hor'>$matin</span>
                <span class='hor'>$soir</span>
                </div> </div> ";
    }
};
?>