create database kobe;
use kobe;

create table Burger (
    id int not null auto_increment primary key,
    imgURL varchar(800),
    titre varchar(255),
    descri varchar(800),
    prix float
);

create table reserv (
    id int not null auto_increment primary key,
    resdate datetime,
    mail varchar(800),
    nombre int
);