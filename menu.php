<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://fonts.google.com/specimen/Shojumaru">

</head>
<body>
<?php
        include "header.php";
?>
    <h2>Nos Burgers</h2>
<div class="burgerscontain">
    <?php
        include "menu/carteburger.php"; 
    ?>
</div>
        
<?php
        include "footer.php";
?>
</body>
</html>