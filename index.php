<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="https://fonts.google.com/specimen/Shojumaru">
    <?php include_once "data.php" ?>
</head>
<body>
<?php 
    if ($_SESSION["admin"]){?>
    <div class="admin">
        <a href="deco.php">Se deconnecter</a>
        <a href="ajoutmenu.php">Ajouter/modifier burger</a>
    </div>

<?php } ?>    
    <!-- affiche la home page -->
    <?php
        include "header.php";
        include "homepage/main.php";
        include "footer.php";
    ?>
    <!-- ------------------ -->
    <script src="index.js"></script>
</body>
</html>